import { useParams, Link } from 'react-router-dom'
import { useState, useEffect} from 'react'

function ShoeDetail () {
    const [shoe, setShoe] = useState(null)
    const { id } = useParams()
    async function getData () {
        const resp = await fetch(`http://localhost:8080/api/shoes/${id}`)
        if (resp.ok) {
            const data = await resp.json()
            setShoe(data)
        }
    }

    useEffect(()=> {
        getData()
    }, [])


    const handleDelete = async () => {
        const url = `http://localhost:8080/api/shoes/${id}`

        const fetchConfigs = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }

        const resp = await fetch(url, fetchConfigs)
        const data = await resp.json()
    }

    return (
        <>
        { shoe &&
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Details</h1>
                    <h4>Name: <span>{shoe.model_name}</span></h4>
                    <h5>Manufacturer: <span>{shoe.manufacturer}</span></h5>
                    <h5>Color: <span>{shoe.color}</span></h5>
                    <p>Closet Name: {shoe.bin.closet_name}</p>
                    <p>Bin Number: {shoe.bin.bin_number}</p>
                    <p>Bin Size: {shoe.bin.bin_size}</p>
                    <Link to='/shoes' className="btn btn-primary">Return to Shoe List</Link>
                    <Link to='/shoes'><button onClick={handleDelete} className="btn btn-danger">Delete Shoe</button></Link>
                </div>
            </div>
        </div>
        }
    </>
);
}

export default ShoeDetail;
