import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import Hats from './Hats';
import Shoes from './Shoes';
import ShoeDetail from './ShoeDetail';
import HatDetail from './HatDetail';
import HatForm from './HatForm';
import ShoeForm from './ShoeForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>

          <Route path="/hats">
          <Route index element={<Hats  />} />
          <Route path=":id" element={<HatDetail  />} />
          <Route path="new" element={<HatForm />} />
          </Route>

          <Route path="/" element={<MainPage />} />

          <Route path="/shoes">
          <Route path="/shoes" element={<Shoes  />} />
          <Route path=":id" element={<ShoeDetail />} />
          <Route path="new" element={<ShoeForm />} />
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
