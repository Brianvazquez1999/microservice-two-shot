import React, {useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function Hats() {
const [hats, setHats] = useState([])

    async function loadHats() {
        const response = await fetch("http://localhost:8090/api/hats/")
        if (response.ok) {
      const data = await response.json()
        setHats(data.hats)

        }
        else {
        }


      }
      useEffect(() => {
        loadHats()
      })
  return (
    <>
      <div className="container">
        <h2>Hats</h2>
        <div className="row">
            {hats.map(hat => {
                return (
                  <div className="col">
                    <div className="card-body">
                    <img src={hat.picture_url} height="150" width="150"/>
              <h5 className="card-title"><Link to={`/hats/${hat.id}`}>{hat.style_name}</Link></h5>
              <h6 className="card-subtitle mb-2 text-muted">
                {hat.fabric}
              </h6>
              <p className="card-text">
                {hat.location.closet_name}
              </p>
            </div>
                    </div>
                )
            })}
        </div>
      </div>
    </>
  );
}

export default Hats
