import React, {useEffect, useState } from 'react';
import { Link } from 'react-router-dom'

function Shoes() {
const [shoes, setShoes] = useState([])

    async function loadShoes() {
        const response = await fetch("http://localhost:8080/api/shoes/")
        if (response.ok) {
      const data = await response.json()
        setShoes(data.shoes)

        }
        else {
          console.log(response)
        }


      }
      useEffect(() => {
        loadShoes()
      })
  return (
    <>
      <div className="container">
        <h2>Shoes</h2>
        <div className="row">
            {shoes.map(shoe => {
                return (
                  <div className="col">
                    <div className="card-body">
                <img src={shoe.picture_url} height="150" width="150"/>
              <h5 className="card-title"><Link to={`${shoe.id}`}>{shoe.model_name}</Link></h5>
              <h6 className="card-subtitle mb-2 text-muted">
                {shoe.manufacturer}
              </h6>
              <p className="card-text">
                {shoe.bin.closet_name}
              </p>
            </div>
                    </div>
                )
            })}
        </div>
      </div>
    </>
  );
}

export default Shoes
