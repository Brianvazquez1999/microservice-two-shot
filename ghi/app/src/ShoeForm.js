import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom'

function ShoeForm() {
    const [bins, setBins] = useState([])
    const [formData, setFormData] = useState({
        manufacturer: "",
        model_name: "",
        color: "",
        picture_url: "",
        bin: {}
    })

    const fetchData = async () => {
        const url = "http://localhost:8100/api/bins/"
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setBins(data.bins)
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()
        const url = `http://localhost:8080${formData.bin}shoes/`

        const fetchConfig = {
            method: "post",
            //Because we are using one formData state object,
            //we can now pass it directly into our request!
            body: JSON.stringify(formData),
            headers: {
              'Content-Type': 'application/json',
            },
          };
          const response = await fetch(url, fetchConfig);

          if (response.ok) {
            setFormData({
                manufacturer: "",
                model_name: "",
                color: "",
                picture_url: "",
                bin: {}

            })
          }


        }

        const handleFormChange = (e) => {
            const value = e.target.value
            const inputName = e.target.name
            setFormData({...formData, [inputName]: value})
        }

        return (
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <h1>Create a new shoe</h1>
                  <form onSubmit={handleSubmit} id="create-hat-form">
                    <div className="form-floating mb-3">
                      <input onChange={handleFormChange} value={formData.manufacturer} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                      <label htmlFor="manufacturer">Manufacturer</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input onChange={handleFormChange} value={formData.model_name} placeholder="Model" required type="text" name="model_name" id="model_name" className="form-control" />
                      <label htmlFor="model_name">Model Name</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input onChange={handleFormChange} value={formData.color} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                      <label htmlFor="ends">Color</label>
                    </div>



                    <div className="form-floating mb-3">
                    <input onChange={handleFormChange} value={formData.picture_url} placeholder="Picture URL" className="form-control" id="picture_url" name="picture_url"  />
                      <label htmlFor="picture_url">Picture URL</label>
                    </div>





                    <div className="form-floating mb-3">
                    <select onChange={handleFormChange} required name="bin" id="bin" className="form-select">
                        <option value="">Choose a bin</option>
                        {bins.map(bin => {
                          return (
                            <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                          )
                        })}
                      </select>
                    </div>
                        <button className="btn btn-primary">Create</button>
                  </form>
                </div>
              </div>
            </div>
          )
        }

        export default ShoeForm
