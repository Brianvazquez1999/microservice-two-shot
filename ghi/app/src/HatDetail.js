import React, {useEffect, useState } from 'react';
import { useParams, Link } from 'react-router-dom'


function HatDetail() {
    const [hat, setHat] = useState(null)
    const { id } = useParams()
   async function fetchData ()  {
        const response = await fetch(`http://localhost:8090/api/hats/${id}`)
        if (response.ok) {
            const data = await response.json()
            setHat(data)
        }
    }


    useEffect(() => {
        fetchData()
    }, [])

    const handleDelete = async () => {
        const url = `http://localhost:8090/api/hats/${id}`

        const fetchConfigs = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }

        const resp = await fetch(url, fetchConfigs)
        const data = await resp.json()
    }

    return(
    <>
    { hat &&
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Hat Detail</h1>
                <h4>Name: <span>{hat.style_name}</span></h4>
                <h4>Color: <span>{hat.color}</span></h4>
                <h4>Fabric: <span>{hat.fabric}</span></h4>
                <h4>Location:  <span>{hat.location.closet_name}</span></h4>

                <Link to='/hats' className="btn btn-primary">Return to Hat list</Link>
                <Link to='/hats'><button onClick={handleDelete} className="btn btn-danger">Delete Hat</button></Link>
            </div>
        </div>
    </div>
}
</>)

}
 export default HatDetail
