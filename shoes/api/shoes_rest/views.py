from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Shoe, BinVO
from common.json import ModelEncoder
from django.http import JsonResponse
import json


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_number",
        "bin_size",
        "import_href",
    ]

class ShoesListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "picture_url",
        "id",
        "bin",
    ]
    encoders = {
        "bin": BinVOEncoder()
    }

class ShoesDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
        "id",
    ]
    encoders = {
        "bin": BinVOEncoder()
    }

@require_http_methods(["GET", "POST"])
def api_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id == None:
            shoes = Shoe.objects.all()
            return JsonResponse({"shoes": shoes}, encoder=ShoesListEncoder, safe=False)
        else:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
            return JsonResponse({"shoes": shoes}, encoder=ShoesListEncoder, safe=False)
    else:
        content = json.loads(request.body)
        try:
            bin_href = f'/api/bins/{bin_vo_id}/'
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse({"message": "Invalid Bin id"}, status=400)
        shoe = Shoe.objects.create(**content)
        return JsonResponse({"shoe": shoe}, encoder=ShoesDetailEncoder, safe=False)




@require_http_methods(["DELETE", "GET"])
def api_shoe(request, id):
    if request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        shoe = Shoe.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoesDetailEncoder,
            safe=False
        )
