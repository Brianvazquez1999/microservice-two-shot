from django.urls import path
from .views import api_list_hats, hat_details

urlpatterns = [
    path("hats/", api_list_hats, name="api_create_hats"),
    path("hats/<int:id>/", hat_details, name="hat_detais" ),
    path("locations/<int:location_vo_id>/hats/", api_list_hats, name="api_list_hats"),
]
