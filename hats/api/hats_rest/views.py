from django.shortcuts import render
from .models import Hats, LocationVo
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
import json

class LocationVoEncoder(ModelEncoder):
    model = LocationVo
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
    ]

class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
        "id"
    ]
    encoders = {
        "location": LocationVoEncoder()
    }



# Create your views here.
@require_http_methods(["GET","POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id == None:
            hats = Hats.objects.all()
            return JsonResponse({"hats":hats}, encoder=HatsListEncoder, safe=False)
        else:
            hats = Hats.objects.filter(location=location_vo_id)
            return JsonResponse({"hats": hats}, encoder=location_vo_id, safe=False)
    else:
        content = json.loads(request.body)
        try:
            location_href = f'/api/locations/{location_vo_id}/'
            location = LocationVo.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVo.DoesNotExist:
            return JsonResponse({"message": "Invalid Location id"}, status=400)
        hat = Hats.objects.create(**content)
        return JsonResponse(hat, encoder=HatsListEncoder, safe=False)
@require_http_methods(["DELETE","GET"])
def hat_details(request, id):
    if request.method == "DELETE":
        count, _ = Hats.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        try:
            hat = Hats.objects.get(id=id)
        except Hats.DoesNotExist:
            return JsonResponse({"message": "Invalid hat id"})

        return JsonResponse(hat, encoder=HatsListEncoder, safe=False)
