from django.db import models

# Create your models here.

class LocationVo(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name=models.CharField(max_length=200)
    section_number=models.PositiveSmallIntegerField()
    shelf_number=models.PositiveSmallIntegerField()


class Hats(models.Model):
    fabric = models.CharField(max_length=150)
    style_name= models.CharField(max_length=150)
    color = models.CharField(max_length=150)
    picture_url=models.CharField(max_length=200)
    location = models.ForeignKey(LocationVo,related_name="location", on_delete=models.CASCADE )
